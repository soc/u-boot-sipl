#include <common.h>
#include <command.h>
#include <dm/device.h>
#include <dm/uclass.h>
#include <sipl.h>

/* -------------------------------------------------------------------------- */

static int do_sipl_tm(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

    struct udevice *dev;
    int ret, len;
    char req[SIPL_MESSAGE_MAXSIZE], rsp[SIPL_MESSAGE_MAXSIZE];

    /* Validate arguments */
    if(argc < 2) return CMD_RET_USAGE;

    /* find and activate SIPL/serial interface */
    if((ret = sipl_activate(CONFIG_SIPL_SERIAL_DEVICE_NAME,&dev))) {
        printf("cannot find UCLASS_SERIAL device \"%s\"\n",CONFIG_SIPL_SERIAL_DEVICE_NAME);
        return CMD_RET_FAILURE;
    }

    /* run SIPL-TM command */
    strcpy(req,argv[1]);
    printf("SIPL-TM request  = \"%s\"\n",req);
    len = strlen(req);
    if((ret = sipl_tm_command(dev,req,rsp,&len))) {
        printf("SIPL-TM: ERROR - %d\n",ret);
        return CMD_RET_FAILURE;
    }
    rsp[len] = '\0';
    printf("SIPL-TM response = \"%s\"\n",rsp);

    return CMD_RET_SUCCESS;
};

/* -------------------------------------------------------------------------- */

static int do_sipl_ipmi(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

    struct udevice *dev;
    int i, ret;
    unsigned int num;
    struct SIPL_IpmiMessage qmsg, rmsg;

    /* Validate arguments */
    if(argc < 3) return CMD_RET_USAGE;

    /* find and activate SIPL/serial interface */
    if((ret = sipl_activate(CONFIG_SIPL_SERIAL_DEVICE_NAME,&dev))) {
        printf("cannot find UCLASS_SERIAL device \"%s\"\n",CONFIG_SIPL_SERIAL_DEVICE_NAME);
        return CMD_RET_FAILURE;
    }

    /* initialise messages */
    sipl_ipmi_message_init(qmsg);
    sipl_ipmi_message_init(rmsg);

    if(sipl_ipmi_read_byte(argv[1],&num)) {
        printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
        return CMD_RET_USAGE;
    }
    qmsg.NetFn = num;
    if(sipl_ipmi_read_byte(argv[2],&num)) {
        printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
        return CMD_RET_USAGE;
    }
    qmsg.cmd = num;
    for(i=3; i<argc; ++i) {
        if(sipl_ipmi_read_byte(argv[i],&num)) {
            printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
            return CMD_RET_USAGE;
        }
        qmsg.data[qmsg.dataLength++] = num;
    }

    /* run IPMI command */
    sipl_ipmi_message_print(qmsg,"IPMI command request:");
    if((ret = sipl_ipmi_command(dev,qmsg,&rmsg))) {
        printf("IPMI: ERROR - %d\n",ret);
        return CMD_RET_FAILURE;
    }
    sipl_ipmi_message_print(rmsg,"IPMI command reqsponse:");

    return CMD_RET_SUCCESS;
};

/* -------------------------------------------------------------------------- */

static int do_sipl_send(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

    struct udevice *dev;
    int i, ret;
    unsigned int num;
    struct SIPL_IpmiMessage qmsg, rmsg;

    /* Validate arguments */
    if(argc < 5) return CMD_RET_USAGE;

    /* find and activate SIPL/serial interface */
    if((ret = sipl_activate(CONFIG_SIPL_SERIAL_DEVICE_NAME,&dev))) {
        printf("cannot find UCLASS_SERIAL device \"%s\"\n",CONFIG_SIPL_SERIAL_DEVICE_NAME);
        return CMD_RET_FAILURE;
    }

    /* initialise messages */
    sipl_ipmi_message_init(qmsg);
    sipl_ipmi_message_init(rmsg);

    if(sipl_ipmi_read_byte(argv[1],&num)) {
        printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
        return CMD_RET_USAGE;
    }
    qmsg.rsSA = num;
    if(sipl_ipmi_read_byte(argv[2],&num)) {
        printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
        return CMD_RET_USAGE;
    }
    qmsg.rqSA = num;
    if(sipl_ipmi_read_byte(argv[3],&num)) {
        printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
        return CMD_RET_USAGE;
    }
    qmsg.NetFn = num;
    if(sipl_ipmi_read_byte(argv[4],&num)) {
        printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
        return CMD_RET_USAGE;
    }
    qmsg.cmd = num;
    for(i=5; i<argc; ++i) {
        if(sipl_ipmi_read_byte(argv[i],&num)) {
            printf("IPMI: ERROR - all numbers must be written as two-digit hexadecimals\n");
            return CMD_RET_USAGE;
        }
        qmsg.data[qmsg.dataLength++] = num;
    }

    /* run SendMessage command */
    sipl_ipmi_message_print(qmsg,"SendMessage request:");
    if((ret = sipl_send_message_command(dev,qmsg,&rmsg))) {
        printf("SendMessage: ERROR - %d\n",ret);
        return CMD_RET_FAILURE;
    }
    sipl_ipmi_message_print(rmsg,"GetMessage  response:");

    return CMD_RET_SUCCESS;
};

/* -------------------------------------------------------------------------- */

static int do_sipl_id(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

    struct udevice *dev;
    unsigned int hw_addr, ipmb_addr, site_number, site_type;
    int num = 1, ret;
    char shelf_addr[SIPL_SHELF_ADDR_MAXSIZE+1];
    int shelf_addr_len;

    /* Validate arguments */
    if(argc < 1) return CMD_RET_USAGE;
    if(argc >= 2) {
        num = strtoul(argv[1],NULL,10);
    }

    /* find and activate SIPL/serial interface */
    if((ret = sipl_activate(CONFIG_SIPL_SERIAL_DEVICE_NAME,&dev))) {
        printf("cannot find UCLASS_SERIAL device \"%s\"\n",CONFIG_SIPL_SERIAL_DEVICE_NAME);
        return CMD_RET_FAILURE;
    }

    for(unsigned int i=0; i<num; ++i) {

        /* GetAddressInfo */
        if(ret = sipl_get_address_info(dev,&hw_addr,&ipmb_addr,&site_number,&site_type)) {
            printf("GetAddressInfo: ERROR - %d\n",ret);
            return CMD_RET_FAILURE;
        }

        /* GetShelfAddressInfo */
        if(ret = sipl_get_shelf_address_info(dev, ipmb_addr, shelf_addr, &shelf_addr_len)) {
            printf("GetShelfAddressInfo: ERROR - %d\n",ret);
            return CMD_RET_FAILURE;
        }
        shelf_addr[shelf_addr_len+1] = '\0';
        printf("IPMC:  addr = %02X, site = (%02X,%02X), shelf = \"%s\"\n",ipmb_addr,site_type,site_number,shelf_addr);
    }

    return CMD_RET_SUCCESS;
}

/* -------------------------------------------------------------------------- */

static cmd_tbl_t cmd_sipl_sub[] = {
    U_BOOT_CMD_MKENT(tm, 3, 1, do_sipl_tm, "", ""),
    U_BOOT_CMD_MKENT(ipmi, CONFIG_SYS_MAXARGS, 1, do_sipl_ipmi, "", ""),
    U_BOOT_CMD_MKENT(send, CONFIG_SYS_MAXARGS, 1, do_sipl_send, "", ""),
    U_BOOT_CMD_MKENT(id, 3, 1, do_sipl_id, "", ""),
};

/* -------------------------------------------------------------------------- */

int do_sipl(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

    cmd_tbl_t *c;

    if(argc < 2) return CMD_RET_USAGE;

    /* Strip off leading \"sipl\" command argument */
    argc--;
    argv++;

    if((c = find_cmd_tbl(argv[0],&cmd_sipl_sub[0],ARRAY_SIZE(cmd_sipl_sub)))) {
        return c->cmd(cmdtp,flag,argc,argv);
    }

    return CMD_RET_USAGE;
}

/* -------------------------------------------------------------------------- */

static char sipl_help_text[] =
    "tm \"[...]\"                        - run terminal mode command\n"
    "sipl ipmi NetFn Cmd data ...           - run IPMI command\n"
    "sipl send rsSA rqSA NetFn Cmd data ... - run SendMessage command\n"
    "sipl id                                - get identifiers\n"
    "\n"
    "Note:\n"
    "All numbers must be written as two-digit hexadecimals.\n"
    "\n"
    "Examples:\n"
    "# Pigeon Point Systems (OEM) GetHardwareAddress:\n"
    "sipl tm \"[B8 00 05 0A 40 00]\"\n"
    "# IPMI Get Device ID:\n"
    "sipl ipmi 06 01\n"
    "# Send Message to ShMgr (e.g. GetShelfAddressInfo):\n"
    "sipl send 20 82 2C 02 00\n"
    "# Get blade and shelf address info (optionally running in a loop, useful for testing):\n"
    "sipl id\n"
    "sipl id 10000\n"
;

/* -------------------------------------------------------------------------- */

U_BOOT_CMD(
    sipl, CONFIG_SYS_MAXARGS, 1, do_sipl,
    "SIPL - Serial Interface Protocol Lite to IPMC",
    sipl_help_text
);
