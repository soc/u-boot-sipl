# u-boot-sipl

SIPL functions for use in U-Boot.

The **Serial Interface Protocol Lite (SIPL)** proposed by Pigeon Point Systems is an implementation of the IPMC serial/modem interface with terminal mode, cf. IPMC v2.0, sect. 14.7. It allows an embedded processor, e.g. an SoC, in the payload to send IPMI commands to the Serial Payload Interface (PI) of the IPMC.

SIPL prerequisites
------------------

Obviously, the PI of the IPMC needs to be connected to the embedded processor. The corresponding UART device needs to be defined and enabled in the device tree of U-Boot, e.g.

`uart1: serial@ff010000 {`  
`    ...`  
`    compatible = "cdns,uart-r1p12", "xlnx,xuartps";`  
`    status = "okay";`  
`    ...`  
`};`

SIPL code & build
-----------------

This git project contains the source code and other files required to add SIPL functionality to u-boot-xlnx. It has been used with version v2019.1, with Yocto, and for a zynqmp_zcu102_rev1.0 as an example. The code provides functions for use during the initialisation as well as commands to be used at the U-Boot command-line interface.

To make the source code available in the U-Boot build you need to add the following files to the u-boot-xlnx source (= git) directory:
* include/sipl.h 
* lib/sipl.c
* cmd/sipl.c

In order to enable building of the software, you need to modify the following files:
* lib/Kconfig
* lib/Makefile
* cmd/Kconfig
* cmd/Makefile

In order to select the SIPL configuration options, you need to modify the board specific file, for the example used in this project:
* configs/xilinx_zynqmp_zcu102_rev1.0_defconfig  
In particular, the following configuration options need to be set:  
`CONFIG_SIPL=y`  
`CONFIG_SIPL_SERIAL_DEVICE_NAME=serial@0xff010000`  
`CONFIG_SIPL_IGNORE_CHK2_ERROR=y`  
`CONFIG_CMD_SIPL=y`  
The configuration option CONFIG_SIPL_SERIAL_DEVICE_NAME selects which serial device to use.  
The configuration option CONFIG_SIPL_IGNORE_CHK2_ERROR enables to ignore the Chk2 checksum calculation error of the GetMessage command, present in v1.2 of the IPMC software.

If you want to use the SIPL code during the U-Boot board initialisation stage, one possibility is to add it ito the misc_init_r() function. For the example used in this project, the following code needs to be copied (or patched):  
* boards/xilinx/zynqmp/zynqmp.c  
In order to include the misc_init_r() function in the initialisation, you need to select the configuration option CONFIG_MISC_INIT_R.  
The configuration option CONFIG_SIPL_DEBUG enables diagnostic tests of the SIPL software to be run in the misc_init_r() function.

This project also provides a sample patch script (scripts/patch.sh) which performs all the steps described above, and works for u-boot-xlnx version v2019.1, and the zynqmp_zcu102_rev1.0.

SIPL functions
--------------

In order to find and activate the serial interface (UART) for use with the SIPL protocol, use the following function:  
`int sipl_activate(const char *name, struct udevice **devp);`

The software is layered and several functions with increasing levels of complexitiy are available to U-Boot in order to send IPMI commands to the IPMC:

* For sending a SIPL-TM command in human-readable text form, use the following function:  
`int  sipl_tm_command(struct udevice *dev, char *req, char *rsp, int* len);`

* For sending an IPMI command to the IPMC, use the following function, specifying the NetFn and Cmd codes of the request message (qmsg). The response will be in the response message. Any response data will be in the data member of the response message (rmsg):  
`int  sipl_ipmi_command(struct udevice *dev, const struct SIPL_IpmiMessage qmsg, struct SIPL_IpmiMessage *rmsg);`

* For sending a command via the blade's IPMC to another IPMC in the shelf (e.g. the ShelfMgr) via the "SendMessage", use the following function, specifying the rsSA (destination) and rqSA (source) source addresses, and the NetFn and Cmd codes of the request message (qmsg). The function will fill in all other fields required, run a "SendMessage" command followed by a "GetMessageFlags" and "GetMessage" command. The response will be provided in the response message (rmsg). This function is similar to using the "-t" option of ipmitool:  
`int  sipl_send_message_command(struct udevice *dev, const struct SIPL_IpmiMessage qmsg, struct SIPL_IpmiMessage *rmsg);`

* For getting the blade and shelf address information (the latter via "SendMessage"), the following two high-level functions are provided:  
`int  sipl_get_address_info(struct udevice *dev, unsigned int *hw_addr, unsigned int *ipmb_addr, unsigned int *site_number, unsigned int* site_type);`  
`int  sipl_get_shelf_address_info(struct udevice *dev, const unsigned int ipmb_addr, char *shelf_addr, int *shelf_addr_len);`

SIPL commands
-------------

To each of the message sending functions presented above, there exists a command to be used at the U-Boot command-line interface:

`U-Boot> sipl tm "<sipl command string>"`  
`U-Boot> sipl ipmi NetFn Cmd data ...`  
`U-Boot> sipl send rsSA rqSA NetFn Cmd data ...`  
`U-Boot> sipl id`  

Note: All numbers must be written as two-digit hexadecimals.

Examples:

* Pigeon Point Systems (OEM) Get Hardware Address:  
`U-Boot> sipl tm \"[B8 00 05 0A 40 00]\"`  
* IPMI Get Device ID :  
`U-Boot> sipl ipmi 06 01`  
* Send Message to ShMgr (e.g. GetShelfAddressInfo):  
`U-Boot> sipl send 20 82 2C 02 00`
* Get blade and shelf address info (optionally running in a loop, useful for testing):  
`U-Boot> sipl id`  
`U-Boot> sipl id 10000`
