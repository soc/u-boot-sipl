#ifdef CONFIG_MISC_INIT_R

#include <common.h>
#include <net.h>
#include <dm.h>
#include <dm/device-internal.h>
#include <serial.h>
#include <sipl.h>

#include <configs/platform-top.h>
#include "sipl_init.h"


// construct_dhcp_clientid sets $dhcp_clientid from IPMC information
// It should be #defined in platform-top.h, but we provide a simple default implementation
// that concatenates the site type, the site number, the IPMB address and the shelf address
#ifndef construct_dhcp_clientid
    #warning Using a default schema for DHCP Client ID
    // do { ... } while (0); is a C idiom to have multiple statements in a #define
    #define construct_dhcp_clientid(hw_addr, ipmb_addr, site_number, site_type, shelf_addr) do { \
        char dhcp_clientid[3*(3+SIPL_SHELF_ADDR_MAXSIZE)]; \
        sprintf(dhcp_clientid, "%02X:%02X:%02X", site_type, site_number, ipmb_addr); \
        char *clientid_cursor = dhcp_clientid + 8; \
        char *shelf_addr_cursor = &shelf_addr[0]; \
        while (*shelf_addr_cursor != '\0') { \
            sprintf(clientid_cursor, ":%02X", *shelf_addr_cursor); \
            clientid_cursor += 3; \
            shelf_addr_cursor++; \
        } \
        env_set("dhcp_clientid", dhcp_clientid); \
    } while (0);
#endif

int sipl_misc_init_r(void) {

    uchar buf[6];

    struct udevice *dev;
    int ret, len;
    char req[SIPL_MESSAGE_MAXSIZE], rsp[SIPL_MESSAGE_MAXSIZE];
    struct SIPL_IpmiMessage qmsg, rmsg;
    unsigned int hw_addr, ipmb_addr, site_number, site_type;
    char shelf_addr[SIPL_SHELF_ADDR_MAXSIZE+1];

    // activate serial device
    if((ret = sipl_activate(CONFIG_SIPL_SERIAL_DEVICE_NAME,&dev))) {
        printf("cannot find UCLASS_SERIAL device \"%s\"\n",CONFIG_SIPL_SERIAL_DEVICE_NAME);
        return 1;
    }
    printf("SIPL:  %s\n",dev->name);

#ifdef CONFIG_SIPL_DIAGNOSTICS
    strcpy(req,"[B0000100]");
    len = strlen(req);
    printf("-------------------- DIAG #1 --------------------\n");
    printf("SIPL-TM request =  \"%s\"\n",req);
    if((ret = sipl_tm_write(dev,req,&len))) {
        printf("ERROR: writing SIPL-TM request \"%s\" - %d\n",req,-ret);
    }
    else {
        len = 64;
        if((ret = sipl_tm_read(dev,rsp,&len))) {
            rsp[len] = '\0';
            printf("ERROR: reading SIPL-TM response \"%s\" - %d\n",rsp,-ret);
        }
        else {
            rsp[len] = '\0';
            printf("SIPL-TM response = \"%s\"\n",rsp);
        }
    }

    strcpy(req,"[B0000100]");
    len = strlen(req);
    printf("-------------------- DIAG #2 --------------------\n");
    printf("SIPL-TM request =  \"%s\"\n",req);
    if((ret = sipl_tm_command(dev,req,rsp,&len))) {
        printf("ERROR: running SIPL-TM command - %d\n",-ret);
    }
    else {
        rsp[len] = '\0';
        printf("SIPL-TM response = \"%s\"\n",rsp);
    }

    strcpy(req,"[18 00 34 00 20 B0 30 82 02 02 00 7A]");
    len = strlen(req);
    printf("-------------------- DIAG #3.1 ------------------\n");
    printf("SIPL-TM request =  \"%s\"\n",req);
    if((ret = sipl_tm_command(dev,req,rsp,&len))) {
        printf("ERROR: running SIPL-TM command - %d\n",-ret);
    }
    else {
        rsp[len] = '\0';
        printf("SIPL-TM response = \"%s\"\n",rsp);
    }

    strcpy(req,"[18 00 31]");
    len = strlen(req);
    printf("-------------------- DIAG #3.2 ------------------\n");
    printf("SIPL-TM request =  \"%s\"\n",req);
    if((ret = sipl_tm_command(dev,req,rsp,&len))) {
        printf("ERROR: running SIPL-TM command - %d\n",-ret);
    }
    else {
        rsp[len] = '\0';
        printf("SIPL-TM response = \"%s\"\n",rsp);
    }

    strcpy(req,"[18 00 33]");
    len = strlen(req);
    printf("-------------------- DIAG #3.2 ------------------\n");
    printf("SIPL-TM request =  \"%s\"\n",req);
    if((ret = sipl_tm_command(dev,req,rsp,&len))) {
        printf("ERROR: running SIPL-TM command - %d\n",-ret);
    }
    else {
        rsp[len] = '\0';
        printf("SIPL-TM response = \"%s\"\n",rsp);
    }

    sipl_ipmi_message_init(qmsg);
    sipl_ipmi_message_init(rmsg);
    qmsg.NetFn = SIPL_NETFN_APP_REQUEST;
    qmsg.cmd = SIPL_CMD_APP_GET_MSG_FLAGS;
    printf("-------------------- DIAG #4.1 ------------------\n");
    sipl_ipmi_message_print(qmsg,"IPMI request:");
    if((ret = sipl_ipmi_command(dev,qmsg,&rmsg))) {
        printf("ERROR: running IPMI command - %d\n",-ret);
    }
    else {
        sipl_ipmi_message_print(rmsg,"IPMI response:");
    }

    printf("-------------------- DIAG #4.2 ------------------\n");
    qmsg.cmd = SIPL_CMD_APP_GET_MESSAGE;
    sipl_ipmi_message_print(qmsg,"IPMI request:");
    if((ret = sipl_ipmi_command(dev,qmsg,&rmsg))) {
        printf("ERROR: running IPMI command - %d\n",-ret);
    }
    else {
        sipl_ipmi_message_print(rmsg,"IPMI response:");
    }

    qmsg.rsSA = SIPL_RSSA_SHMGR;
    qmsg.rqSA = 0x82;
    qmsg.NetFn = SIPL_NETFN_PICMG_REQUEST;
    qmsg.cmd = SIPL_CMD_PICMG_GET_SHMGR_ADDR;
    qmsg.data[qmsg.dataLength++] = SIPL_DATA_PICMG_IDENTIFIER;
    printf("-------------------- DIAG #5 --------------------\n");
    sipl_ipmi_message_print(qmsg,"SendMessage request:");
    if((ret = sipl_send_message_command(dev,qmsg,&rmsg))) {
    }
    else {
        sipl_ipmi_message_print(rmsg,"SendMessage response:");
    }
#endif  /* CONFIG_SIPL_DIAGNOSTICS */

    ret = 0;
    if(sipl_get_address_info(dev,&hw_addr,&ipmb_addr,&site_number,&site_type)) {
        printf("ERROR: reading IPMI address info\n");
        ret = -1;
    }
    if(sipl_get_shelf_address_info(dev, ipmb_addr, shelf_addr, &len)) {
        printf("ERROR: reading IPMI shelf address info\n");
        ret = -1;
    }
    if(!ret) {
        shelf_addr[len+1] = '\0';
        printf("IPMC:  addr = %02X, site = (%02X,%02X), shelf = \"%s\"\n",ipmb_addr,site_type,site_number,shelf_addr);
#if defined(CONFIG_DHCP_CLIENTID)
        construct_dhcp_clientid(hw_addr, ipmb_addr, site_number, site_type, shelf_addr);
#endif
    }

    return(0);
}
#endif
