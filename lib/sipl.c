/* --------------------------------------------------------------------------
 * file: lib/sipl.h
 * desc: SIPL-TM protocol functions
 * auth: 15-JUL-2020 R. Spiwoks
 * -------------------------------------------------------------------------- */

#include <dm/device.h>
#include <dm/device-internal.h>
#include <dm/uclass.h>
#include <serial.h>
#include <sipl.h>

/* -------------------------------------------------------------------------- */

int sipl_activate(const char *name, struct udevice **devp) {

    struct uclass *uc;
    struct udevice *dev;
    int ret;

    *devp = NULL;

    /* check device name */
    if(name[0] == '\0') {
        printf("ERROR: udevice name empty\n");
        return -EINVAL;
    }

    /* find UCLASS_SERIAL */
    if((ret = uclass_get(UCLASS_SERIAL,&uc))) {
        printf("ERROR: no UCLASS_SERIAL\n");
        return ret;
    }

    /* find udevice */
    uclass_foreach_dev(dev,uc) {
        sipl_debug("dev = \"%s\", name = \"%s\"\n",dev->name,name);
        if(!strcmp(dev->name,name)) {

            /* probe udevice, and acitvate it */
            if((ret = device_probe(dev))) {
                printf("ERROR: activating udevice \"%s\"\n",name);
                return ret;
            }
            *devp = dev;
            return 0;
        }
    }

    printf("ERROR: finding udevice \"%s\"\n",name);
    return -ENODEV;
}

/* -------------------------------------------------------------------------- */

int sipl_tm_write(struct udevice *dev, const char *req, int *len) {

    int idx, ret;

    idx = 0;
    while(true) {
        sipl_debug("req = \"%c\"\n",req[idx]);

        /* write request character by character */
        if((ret = serial_get_ops(dev)->putc(dev,req[idx]))) {
            printf("ERROR: writing character to udevice \"%s\"\n",dev->name);
            *len = idx;
            return ret;
        }
        if((++idx) >= *len) break;
    }

    /* write line feed */
    if((ret = serial_get_ops(dev)->putc(dev,'\n'))) {
        printf("ERROR: writing line feed to udevice \"%s\"\n",dev->name);
        return ret;
    }

    /* set output length */
    *len = idx;
    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_tm_read(struct udevice *dev, char *rsp, int* len) {

    int idx, tim, ret;

    idx = 0;
    while(true) {
        tim = 0;
        while(true) {

            /* read character */
            if((ret = serial_get_ops(dev)->getc(dev)) < 0) {

                /* poll again if necessary */
                if(ret == -EAGAIN) {
                    if((++tim) >= SIPL_READ_TIMEOUT) break;
                    continue;
                } 
            }
            break;
        }

        /* check error */
        if(ret < 0) {
            if(ret == -EAGAIN) {
                sipl_debug("timeout reading character %d from udevice \"%s\"\n",idx,dev->name);
            }
            else {
                printf("ERROR: reading character %d from udevice \"%s\" - %d\n",idx,dev->name,-ret);
            }
            *len = idx;
            return ret;
        }
        sipl_debug("rsp[%d] = %02X, \"%c\"\n",idx,(unsigned int)ret,ret);
        rsp[idx] = ret;

        /* check number of characters */
        if((++idx) >= *len) break;
    }

    /* set output length */
    *len = idx;
    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_tm_command(struct udevice *dev, char *req, char *rsp, int* len) {

    int idx, tim, rln, rdx, ret;
    char buf[SIPL_MESSAGE_MAXSIZE];

    /* write array of characters */
    if((ret = sipl_tm_write(dev,req,len))) {
        printf("ERROR: writing request to udevice \"%s\"\n",dev->name);
        *len = 0;
        return ret;
    }

    idx = 0;
    tim = 0;
    while(true) {

        /* read array of characters */
        rln = SIPL_MESSAGE_MAXSIZE;
        if((ret = sipl_tm_read(dev,buf,&rln))) {
            if(ret != -EAGAIN) {
                printf("ERROR: reading response from udevice \"%s\"\n",dev->name);
                *len = idx;
                return ret;
            }
        }

        /* scan input and skip non-printable characters */
        for(rdx=0; rdx<rln; ++rdx) {
            if(((unsigned int)buf[rdx] < 0x20) || ((unsigned int)buf[rdx] > 0x7e)) continue;
            rsp[idx++] = buf[rdx];
            if(idx >= SIPL_MESSAGE_MAXSIZE) {
                *len = idx;
                return -ENOMEM;
            }
        }

        /* skip echo of request */
        if((idx >= *len) && !strncmp(rsp,req,*len)) {
            idx -= (*len);
            strncpy(rsp,&rsp[*len],idx);
        }

        /* repeat reading until full response */
        if((idx > 0) && (rsp[idx-1] == ']')) break;
        if((++tim) >= SIPL_RESPONSE_TIMEOUT) {
            printf("ERROR: timeout reading response from udevice \"%s\"\n",dev->name);
            return -ETIME;
        }
    }

    /* set output length */
    *len = idx;
    return 0;
}

/* -------------------------------------------------------------------------- */

void sipl_ipmi_message_print(const struct SIPL_IpmiMessage msg, const char *str) {

    unsigned int i;

    printf("%s\n",str);
    printf("rsSA = %02X",msg.rsSA);
    printf(", NetFn/rsLUN = %02X/%1X",msg.NetFn,msg.rsLUN);
    printf(", rqSA = %02X",msg.rqSA);
    printf(", rqSeq/rqLUN = %02X/%1X",msg.rqSeq,msg.rqLUN);
    printf(", cmd = %02X",msg.cmd);
    if(msg.NetFn & SIPL_NETFN_RESPONSE_MASK) {
        printf(", compl = %02X",msg.completionCode);
    }
    if(msg.dataLength) {
        printf(", data =");
        for(i=0; i<msg.dataLength; ++i) {
            printf(" %02X",msg.data[i]);
        }
    }
    printf("\n");
}

/* -------------------------------------------------------------------------- */

int sipl_ipmi_write_request(const struct SIPL_IpmiMessage msg, char *req, int *len) {

    unsigned char byt;
    unsigned int idx = 0;

    // start
    req[idx++] = '[';

    // NetFn/rsLUN
    byt = ((msg.NetFn << 2) | msg.rsLUN) & 0xff;
    sprintf(&req[idx],"%02X",(unsigned int)byt);
    idx += 2;
    req[idx++] = ' ';

    // rqSeq/rqLUN
    byt = ((msg.rqSeq << 2) | msg.rqLUN) & 0xff;
    sprintf(&req[idx],"%02X",(unsigned int)byt);
    idx += 2;
    req[idx++] = ' ';

    // cmd
    byt = msg.cmd;
    sprintf(&req[idx],"%02X",(unsigned int)byt);
    idx += 2;

    // data
    for(unsigned int i=0; i<msg.dataLength; i++) {
        sprintf(&req[idx],"%02X",(unsigned int)msg.data[i]);
        idx += 2;
    }

    // stop
    req[idx++] = ']';
    *len = idx;

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_ipmi_read_byte(const char byt[2], unsigned int *num) {

    char* end;

    // try to convert string to integer
    errno = 0;
    *num = strtoul(byt,&end,16);
    if(end != &byt[2]) {
        return -EINVAL;
    }
    else if(errno == ERANGE) {
        return -ERANGE;
    }
    else if(errno == EINVAL) {
        return -EINVAL;
    }

    return 0;
}

/* -------------------------------------------------------------------------- */

void sipl_ipmi_print_error(const char *rsp, const int len, const char *err) {

    char str[SIPL_MESSAGE_MAXSIZE];

    strncpy(str,rsp,len);
    str[len] = '\0';
    printf("ERROR: rsp = \"%s\" - %s\n",str,err);
}

/* -------------------------------------------------------------------------- */

int sipl_ipmi_read_response(const char *rsp, struct SIPL_IpmiMessage *msg, int *len) {

    unsigned int num , idx;

    msg->dataLength = 0;

    // get start and check if error
    if(rsp[0] != '[') {
        sipl_ipmi_print_error(rsp,*len,"response does not begin with \"[\"");
        return -EINVAL;
    }
    if(!strncmp(rsp,"[ERR ",5)) {
        if(sipl_ipmi_read_byte(&rsp[5],&num)) {
            sipl_ipmi_print_error(rsp,*len,"response has unreadable errCode value");
            return -EINVAL;
        }
        msg->completionCode = num;
        if(rsp[7] != ']') {
            sipl_ipmi_print_error(rsp,*len,"response does not end with \"]\"");
            return - EINVAL;
        }
        return 0;
    }

    // get NetFn/rsLUN
    if(sipl_ipmi_read_byte(&rsp[1],&num)) {
        sipl_ipmi_print_error(rsp,*len,"response has unreadable NetFN/rsLUN value");
        return -EINVAL;
    }
    msg->rsLUN = num & 0x03;
    msg->NetFn = (num >> 2) & 0xff;
    if(rsp[3] != ' ') {
        sipl_ipmi_print_error(rsp,*len,"response is missing a space after NetFN/rsLUN");
        return -EINVAL;
    }

    // get rqSeq/rqLUN
    if(sipl_ipmi_read_byte(&rsp[4],&num)) {
        sipl_ipmi_print_error(rsp,*len,"response has unreadable rqSeq/rqLUN value");
        return -EINVAL;
    }
    msg->rqLUN = num & 0x03;
    msg->rqSeq = (num >> 2) & 0xff;
    if(rsp[6] != ' ') {
        sipl_ipmi_print_error(rsp,*len,"response is missing a space after rqSeq/rqLUN");
        return -EINVAL;
    }

    // get cmd
    if(sipl_ipmi_read_byte(&rsp[7],&num)) {
        sipl_ipmi_print_error(rsp,*len,"response has unreadable cmd value");
        return -EINVAL;
    }
    msg->cmd = num;
    if(rsp[9] != ' ') {
        sipl_ipmi_print_error(rsp,*len,"response is missing a space after cmd");
        return -EINVAL;
    }

    // get completionCode
    if(sipl_ipmi_read_byte(&rsp[10],&num)) {
        sipl_ipmi_print_error(rsp,*len,"response has unreadable completionCode value");
        return -EINVAL;
    }
    msg->completionCode = num;

    idx = 12;
    while(rsp[idx] == ' ') {
        if(sipl_ipmi_read_byte(&rsp[idx+1],&num)) {
            sipl_ipmi_print_error(rsp,*len,"response has unreadable data value");
            return -EINVAL;
        }
        msg->data[msg->dataLength++] = num;
        idx += 3;
    }
    if(rsp[idx] != ']') {
        sipl_ipmi_print_error(rsp,*len,"response does not end with \"]\"");
        return -EINVAL;
    }

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_ipmi_command(struct udevice *dev, const struct SIPL_IpmiMessage qmsg, struct SIPL_IpmiMessage *rmsg) {

    char req[SIPL_MESSAGE_MAXSIZE], rsp[SIPL_MESSAGE_MAXSIZE];
    int len, ret;

    /* translate request message to terminal mode string */
    sipl_ipmi_write_request(qmsg,req,&len);

    /* run terminal mode command */
    if((ret = sipl_tm_command(dev,req,rsp,&len))) {
        printf("ERROR: running SIPL TM command\n");
        return ret;
    }

    /* translate terminal mode string to response message */
    if((ret = sipl_ipmi_read_response(rsp,rmsg,&len))) {
        printf("ERROR: reading SIPL TM response\n");
        return ret;
    }

    /* check completion code */
    if(rmsg->completionCode) {
        printf("ERROR: received SIPL TM response completion code %02X\n",rmsg->completionCode);
        return ret;
    }

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_send_message_write_request(struct SIPL_IpmiMessage *smsg, const struct SIPL_IpmiMessage msg) {

    unsigned char byt, sum = 0;

    // channel: 0 = primary IPMB, IPMI 2.0, 6.3
    byt = 0;
    smsg->data[smsg->dataLength++] = 0;
    sum += byt;

    // rsSA
    byt = msg.rsSA;
    smsg->data[smsg->dataLength++] = byt;
    sum += byt;

    // NetFn/rsLUN
    byt = (msg.NetFn << 2) | (msg.rsLUN & 0x03);
    smsg->data[smsg->dataLength++] = byt;
    sum += byt;

    // chk1
    byt = sipl_ipmi_message_checksum(sum);
    smsg->data[smsg->dataLength++] = byt;
    sum = 0;

    // rqSA
    byt = msg.rqSA;
    smsg->data[smsg->dataLength++] = byt;
    sum += byt;

    // rqSeq/rqLUN
    byt = (msg.rqSeq << 2) | (SIPL_RQLUN_SEND_MESSAGE & 0x03); // IPMI 2.0, 3.2.1, Paragraph 68 

    smsg->data[smsg->dataLength++] = byt;
    sum += byt;

    // cmd
    byt = msg.cmd;
    smsg->data[smsg->dataLength++] = byt;
    sum += byt;

    // data
    for(unsigned int i=0; i<msg.dataLength; i++) {
        byt = msg.data[i];
        smsg->data[smsg->dataLength++] = byt;
        sum += byt;
    }

    // chk2
    byt = sipl_ipmi_message_checksum(sum);
    smsg->data[smsg->dataLength++] = byt;

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_get_message_read_response(const struct SIPL_IpmiMessage tmsg, struct SIPL_IpmiMessage *msg) {

    unsigned char byt, sum = SIPL_RSSA_SHMGR; // IPMI 2.0, 22.6, Table 22, Foot note 1

    msg->dataLength = 0;

    if(tmsg.dataLength < SIPL_SEND_MESSAGE_MINSIZE) {
        printf("ERROR: \"Get Message\" response has wrong size %d - expected at least %d\n",tmsg.dataLength,SIPL_SEND_MESSAGE_MINSIZE);
        return -EINVAL;
    }

    // channel: 0 = primary IPMB, IPMI 2.0, 6.3
    byt = tmsg.data[0];
    if(byt != 0) {
        printf("ERROR: \"Get Message\" response has wrong channel %02X - expected %02X\n",(unsigned int)byt,0);
        return -EINVAL;
    }
    sum += byt;

    // NetFn/rqLUN
    byt = tmsg.data[1];
    sum += byt;
    msg->rqLUN = byt & 0x3;
    msg->NetFn = byt >> 2;
    
    // chk1
    byt = tmsg.data[2];
    if(byt != sipl_ipmi_message_checksum(sum)) {
        printf("ERROR: \"Get Message\" response has wrong Chk1 %02X - expected %02X\n",(unsigned int)byt,(unsigned int)sipl_ipmi_message_checksum(sum));
        return -EINVAL;
    }
    sum = 0;
    
    // rsSA
    byt = tmsg.data[3];
    sum += byt;
    msg->rsSA = byt;

    // rqSeq/rsLUN
    byt = tmsg.data[4];
    sum += byt;
    msg->rsLUN = byt & 0x3;
    msg->rqSeq = byt >> 2;

    // command
    byt = tmsg.data[5];
    sum += byt;
    msg->cmd = byt;

    // completion code
    byt = tmsg.data[6];
    sum += byt;
    msg->completionCode = byt;

    // data
    for(unsigned int i=7; i<(tmsg.dataLength-1); ++i) {
        byt = tmsg.data[i];    
        sum += byt;
        msg->data[msg->dataLength++] = byt;
    }

    // chk2
    byt = tmsg.data[tmsg.dataLength-1];
#ifndef CONFIG_SIPL_IGNORE_CHK2_ERROR
    if(byt != sipl_ipmi_message_checksum(sum)) {
        printf("ERROR: \"Get Message\" response has wrong Chk2 %02X - expected %02X\n",(unsigned int)byt,(unsigned int)sipl_ipmi_message_checksum(sum));
        return -EINVAL;
    }
#endif

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_send_message_command(struct udevice *dev, const struct SIPL_IpmiMessage qmsg, struct SIPL_IpmiMessage *rmsg) {

    struct SIPL_IpmiMessage smsg, tmsg;
    unsigned int loop = 0, ret;

    /* initialise messages */
    sipl_ipmi_message_init(smsg);
    sipl_ipmi_message_init(tmsg);
    smsg.NetFn = SIPL_NETFN_APP_REQUEST;
    smsg.cmd = SIPL_CMD_APP_SEND_MESSAGE;

    /* write request message into SendMessage data */
    sipl_send_message_write_request(&smsg,qmsg);
    sipl_debug_message(smsg,"SendMessage - request:");

    /* run SendMessage command */
    if((ret = sipl_ipmi_command(dev,smsg,&tmsg))) {
        printf("ERROR: running \"Send Message\" command\n");
        return ret;
    }
    sipl_debug_message(tmsg,"SendMessage - response:");

    /* run GetMessageFlags command */
    smsg.NetFn = SIPL_NETFN_APP_REQUEST;
    smsg.cmd = SIPL_CMD_APP_GET_MSG_FLAGS;
    smsg.dataLength = 0;
    while(true) {
    sipl_debug_message(smsg,"GetMessageFlags - request:");
        if((ret = sipl_ipmi_command(dev,smsg,&tmsg))) {
            printf("ERROR: running \"Get Message Flags\" command\n");
            return ret;
        }
        sipl_debug_message(tmsg,"GetMessageFlags - response:");
        if(tmsg.dataLength != 1) {
            printf("ERROR: \"Get Message Flags\" response has wrong size %d - expected %d\n",tmsg.dataLength,1);
            return ret;
        }
        if(tmsg.data[0] == SIPL_GET_MESSAGE_AVAILABLE) break;
        if((++loop) >= SIPL_GET_MESSAGE_TIMEOUT) {
            printf("ERROR: timeout waiting for Receive Message available\n");
            return -ETIME;
        }
    }

    /* run GetMessage command */
    smsg.NetFn = SIPL_NETFN_APP_REQUEST;
    smsg.cmd = SIPL_CMD_APP_GET_MESSAGE;
    smsg.dataLength = 0;
    sipl_debug_message(smsg,"GetMessage - request:");
    if((ret = sipl_ipmi_command(dev,smsg,&tmsg))) {
        printf("ERROR: running \"Get Message\" command\n");
        return ret;
    }
    sipl_debug_message(tmsg,"GetMessage - response:");

    /* read response message from GetMessage data */
    if((ret = sipl_get_message_read_response(tmsg,rmsg))) {
        printf("ERROR: reading response message from \"Get Message\" command\n");
        return ret;
    }

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_get_address_info(struct udevice *dev, unsigned int *hw_addr, unsigned int *ipmb_addr, unsigned int *site_number, unsigned int* site_type) {

    struct SIPL_IpmiMessage qmsg, rmsg;
    int ret;

    /* initialise messages */
    sipl_ipmi_message_init(qmsg);
    sipl_ipmi_message_init(rmsg);

    /* run PICMG 3.0 Get Address Info command */
    qmsg.NetFn = SIPL_NETFN_PICMG_REQUEST;
    qmsg.cmd = SIPL_CMD_PICMG_GET_ADDR;
    qmsg.data[qmsg.dataLength++] = SIPL_DATA_PICMG_IDENTIFIER;
    sipl_debug_message(qmsg,"request:");
    if((ret = sipl_ipmi_command(dev,qmsg,&rmsg))) {
        printf("ERROR: running \"GetShelfAddressInfo\" command\n");
        return ret;
    }
    sipl_debug_message(rmsg,"response:");
    if(rmsg.dataLength != 7) {
        printf("ERROR: \"GetAddressInfo\" response has wrong size %d - expected at least %d\n",rmsg.dataLength,7);
        return -EINVAL;
    }
    if(rmsg.data[0]) {
        printf("ERROR: \"GetAddressInfo\" response has PICMG identifier %02X - expected at least %02X\n",rmsg.data[0],0);
        return -EINVAL;
    }
    *hw_addr = rmsg.data[1];
    *ipmb_addr = rmsg.data[2];

    if((ret = sipl_get_address_info_sh_mgr(dev, *ipmb_addr, site_number, site_type)) != 0) {
        printf("ERROR: sending PICMG GetAddressInfo command to ShelfMgr \"%s\"\n",rmsg.data);
        return ret;
    }

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_get_address_info_sh_mgr(struct udevice *dev, const unsigned int ipmb_addr, unsigned int *site_number, unsigned int *site_type) {
    struct SIPL_IpmiMessage qmsg, rmsg;
    int ret;

    /* initialise messages */
    sipl_ipmi_message_init(qmsg);
    sipl_ipmi_message_init(rmsg);

    /* run PICMG 3.0 Get Shelf Address Info command */
    qmsg.rsSA = SIPL_RSSA_SHMGR;
    qmsg.rqSA = ipmb_addr & 0xff;
    qmsg.NetFn = SIPL_NETFN_PICMG_REQUEST;
    qmsg.cmd = SIPL_CMD_PICMG_GET_ADDR;
    qmsg.data[qmsg.dataLength++] = SIPL_DATA_PICMG_IDENTIFIER;
    qmsg.data[qmsg.dataLength++] = 0x00; // FRU
    qmsg.data[qmsg.dataLength++] = SIPL_CMD_APP_GET_ADDR_KEY_IPMB;
    qmsg.data[qmsg.dataLength++] = ipmb_addr;

    sipl_debug_message(qmsg,"request:");
    if((ret = sipl_send_message_command(dev,qmsg,&rmsg))) {
        printf("ERROR: sending \"GetShelfAddressInfo\" command to ShelfMgr\n");
        return ret;
    }
    sipl_debug_message(rmsg,"response:");
    if(rmsg.dataLength < 7) {
        printf("ERROR: \"GetShelfAddressInfo\" response has wrong size %d - expected at least %d\n",rmsg.dataLength,7);
        return -EINVAL;
    }
    if(rmsg.data[0] != SIPL_DATA_PICMG_IDENTIFIER) {
        printf("ERROR: \"GetShelfAddressInfo\" response has PICMG identifier %02X - expected at least %02X\n",rmsg.data[0],SIPL_DATA_PICMG_IDENTIFIER);
        return -EINVAL;
    }
    if(rmsg.data[2] != ipmb_addr) {
        printf("ERROR: \"GetAddressInfo\" response from ShelfMgr has wrong hw address %02X - expected %02X\n",rmsg.data[2],ipmb_addr);
        return -EINVAL;
    }
    *site_number = rmsg.data[5];
    *site_type = rmsg.data[6];

    return 0;
}

/* -------------------------------------------------------------------------- */

int sipl_get_shelf_address_info(struct udevice *dev, const unsigned int ipmb_addr, char *shelf_addr, int *shelf_addr_len) {

    struct SIPL_IpmiMessage qmsg, rmsg;
    int len, ret;

    /* initialise messages */
    sipl_ipmi_message_init(qmsg);
    sipl_ipmi_message_init(rmsg);

    /* run PICMG 3.0 Get Shelf Address Info command */
    qmsg.rsSA = SIPL_RSSA_SHMGR;
    qmsg.rqSA = ipmb_addr & 0xff;
    qmsg.NetFn = SIPL_NETFN_PICMG_REQUEST;
    qmsg.cmd = SIPL_CMD_PICMG_GET_SHMGR_ADDR;
    qmsg.data[qmsg.dataLength++] = SIPL_DATA_PICMG_IDENTIFIER;
    sipl_debug_message(qmsg,"request:");
    if((ret = sipl_send_message_command(dev,qmsg,&rmsg))) {
        printf("ERROR: sending \"GetShelfAddressInfo\" command\n");
        return ret;
    }
    sipl_debug_message(rmsg,"response:");
    if(rmsg.dataLength < 3) {
        printf("ERROR: \"GetShelfAddressInfo\" response has wrong size %d - expected at least %d\n",rmsg.dataLength,3);
        return -EINVAL;
    }
    if(rmsg.data[0]) {
        printf("ERROR: \"GetShelfAddressInfo\" response has PICMG identifier %02X - expected at least %02X\n",rmsg.data[0],0);
        return -EINVAL;
    }
    if((rmsg.data[1] & SIPL_IPMI_TYPE_MASK) != SIPL_IPMI_TYPE_LANGCODE) {
        printf("ERROR: \"GetShelfAddressInfo\" response has type %02X - expected %02X\n",rmsg.data[1]&SIPL_IPMI_TYPE_MASK,SIPL_IPMI_TYPE_LANGCODE);
        return -EINVAL;
    }
    len = rmsg.data[1] & SIPL_IPMI_LENGTH_MASK;
    if(len > SIPL_SHELF_ADDR_MAXSIZE) {
        printf("ERROR: \"GetShelfAddressInfo\" response has length %02X greater than maximum shelf address size %02X\n",len,SIPL_SHELF_ADDR_MAXSIZE);
        return -EINVAL;
    }
    if(len > (rmsg.dataLength - 2)) {
        printf("ERROR: \"GetShelfAddressInfo\" response has length %02X greater than message %02X\n",len,rmsg.dataLength-2);
        return -EINVAL;
    }
    memcpy(shelf_addr,&rmsg.data[2],len);
    *shelf_addr_len = len;

    return 0;
}
