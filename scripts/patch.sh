#!/bin/sh

# script to link and patch u-boot-xlnx source directory in Yocto, with code from u-boot-sipl repository
cd ${UBOOT_SOFT_DIR}

ln -fs ${SIPL_SOFT_DIR}/include/sipl.h include/sipl.h
ln -fs ${SIPL_SOFT_DIR}/lib/sipl.c lib/sipl.c
ln -fs ${SIPL_SOFT_DIR}/cmd/sipl.c cmd/sipl.c

patch -p 1 < ${SIPL_SOFT_DIR}/lib/Kconfig.patch
patch -p 1 < ${SIPL_SOFT_DIR}/lib/Makefile.patch
patch -p 1 < ${SIPL_SOFT_DIR}/cmd/Kconfig.patch
patch -p 1 < ${SIPL_SOFT_DIR}/cmd/Makefile.patch
patch -p 1 < ${SIPL_SOFT_DIR}/configs/xilinx_zynqmp_zcu102_rev1_0_defconfig.patch

ln -fs ${SIPL_SOFT_DIR}/board/zynqmp.c board/xilinx/zynqmp/zynqmp.c
touch board/xilinx/zynqmp/zynqmp.c
#patch -p 1 < ${SIPL_SOFT_DIR}/board/zynqmp.c.patch

#cd ${YOCTO_BUILD_DIR}; bitbake u-boot-xlnx -C compile
