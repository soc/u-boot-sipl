#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $SCRIPT_DIR

if [ -z $META_UBOOT_DIR ]; then
    echo "META_UBOOT_DIR not set"
    exit -1
fi

function insert_after_configure {
#    sed -i '/do_configure_append () {/a '"$1" $META_UBOOT_DIR/u-boot-xlnx_%.bbappend
    sed -i '/marker for moving local source files/i '"$1" $META_UBOOT_DIR/u-boot-xlnx_%.bbappend
}

function insert_before_configure {
#    sed -i '/do_configure_append () {/i '"$1" $META_UBOOT_DIR/u-boot-xlnx_%.bbappend
    src_uri="$(echo $1 | sed "s/\//\\\\\\//g")"
    sed -i '/SRC_URI/{ $! {N; s/\(.*\)\n$/\1\n'"$src_uri"'\n/; P; D }}' $META_UBOOT_DIR/u-boot-xlnx_%.bbappend
}

function add_file_extra_path {
    fullpath=`readlink -f $SCRIPT_DIR/..`
    sed -i '/FILESEXTRAPATHS:prepend:zynqmp/i '"FILESEXTRAPATHS:append := \":$fullpath\"" $META_UBOOT_DIR/u-boot-xlnx_%.bbappend
}

function install_file {
    add_file $1
    insert_after_configure "\ \ \ \ install \${WORKDIR}/$1 \${S}/$1"
}

function add_file {
    fullpath=`readlink -f $SCRIPT_DIR/../$1`
    mkdir -p `dirname $META_UBOOT_DIR/files/$1`
    cp $fullpath $META_UBOOT_DIR/files/$1
    insert_before_configure "SRC_URI:append = \" file://$1\""
}

add_file_extra_path 
add_file cmd/Kconfig-cmd.patch
add_file cmd/Makefile-cmd.patch
install_file cmd/sipl.c

add_file lib/Makefile-lib.patch
add_file lib/Kconfig-lib.patch
install_file lib/sipl.c

install_file include/sipl.h
install_file lib/sipl_init.c
install_file include/sipl_init.h
